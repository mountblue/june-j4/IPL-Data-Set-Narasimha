const express = require("express");
const path = require("path");
const fs = require("fs");
const matches = path.resolve("CSV/matches.csv");
const deliveries = path.resolve("CSV/deliveries.csv");
const fileName = path.resolve("app.js");
const operations = require(fileName);

const app = express();
app.use("/assets", express.static("assets"));
app.get("/", (req, res) => {
    res.sendFile(__dirname + "/index.html");
})

app.get("/getMatchNumbers", async (req, res) => {
    const data = await operations.getMatchNumbers(matches);
    res.send(data);
});

app.get("/getMatchesWon", async (req, res) => {
    const data = await operations.getMatchesWon(matches);
    res.send(data);
});

app.get("/extraRuns", async (req, res) => {
    const data = await operations.extraRuns(matches, deliveries, "2016");
    res.send(data);
});

app.get("/getEconimicalBowlers", async (req, res) => {
    const data = await operations.getEconimicalBowlers(matches, deliveries, "2015");
    res.send(data);
});

app.get("/getMostMOTM", async (req, res) => {
    const data = await operations.getMostMOTM(matches);
    res.send(data);
});

app.listen(8080);
console.log("Server running...");