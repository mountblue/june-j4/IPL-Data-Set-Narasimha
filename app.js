var fs = require("fs");
let path = require("path");
let matchesFile = path.resolve("CSV/matches.csv");
let deliveriesFile = path.resolve("CSV/deliveries.csv");


// 1. Plot the number of matches played per year of all the years in IPL.
function getMatchNumbers(dataSet) {
    return new Promise(function (resolve, reject) {
        let matchPerSeason = {};
        fs.readFile(dataSet, function (err, data) {
            if (err) {
                reject(err);
            } else {
                data.toString().split("\n").forEach(function (line, index, arr) {
                    if (index != 0) {
                        const match = line.split(",");
                        const season = match[1];
                        if (matchPerSeason.hasOwnProperty(season)) {
                            matchPerSeason[season]++;
                        } else {
                            matchPerSeason[season] = 1;
                        }
                    }
                })
            }
            resolve(matchPerSeason);
        })
    })
}


// 2. Plot a stacked bar chart of matches won of all teams over all the years of IPL.
function getMatchesWon(dataSet) {
    let teamNames = {};
    return new Promise(function (resolve, reject) {
        fs.readFile(dataSet, function (err, data) {
            if (err) {
                reject(err);
            } else {
                data.toString().split("\n").forEach(function (line, index, arr) {
                    if (index !== 0) {
                        const match = line.split(",");
                        const season = match[1];
                        const winner = match[2];
                        if (teamNames.hasOwnProperty(winner)) {
                            if (teamNames[winner].hasOwnProperty(season)) {
                                teamNames[winner][season]++;
                            } else {
                                teamNames[winner][season] = 1;
                            }
                        } else {
                            teamNames[winner] = {};
                            teamNames[winner][season] = 0;
                        }
                    }
                })
            }
            resolve(teamNames);
        })
    })
}


//Function to return the match IDs for Question 3 and Question 4.
function getMatchID(matches, year) {
    var path = require("path");
    var matches = path.resolve("CSV/matches.csv");
    var matchid = [];
    return new Promise(function (resolve, reject) {
        fs.readFile(matches, function (err, matches) {
            matches.toString().split("\n").forEach(function (line, index, arr) {
                if (index !== 0) {
                    const match = line.split(",");
                    if (year == match[1]) {
                        matchid.push(match[0]);
                    }
                }
            })
            resolve(matchid);
        })

    })
}


// 3. For the year 2016 plot the extra runs conceded per team.
function extraRuns(matches, deliveriesFileName, year) {
    return new Promise(async function (resolve, reject) {
        let extraRunsPerTeam = {};
        let matchesID = await getMatchID(matches, year);
        fs.readFile(deliveriesFileName, function (err, data) {
            if (err) {
                reject(err)
            } else {
                data.toString().split("\n").forEach(function (line, index, arr) {
                    if (index !== 0) {
                        const delivery = line.split(",");
                        if (matchesID.includes(delivery[0])) {
                            const bowlingTeam = delivery[3];
                            const extraRuns = delivery[16];
                            if (extraRunsPerTeam.hasOwnProperty(bowlingTeam)) {
                                extraRunsPerTeam[bowlingTeam] += Number(extraRuns);
                            } else {
                                extraRunsPerTeam[bowlingTeam] = Number(extraRuns);
                            }
                        }
                    }
                })
            }
            resolve(extraRunsPerTeam);
        })
    })
}


// 4. For the year 2015 plot the top economical bowlers.
function getEconimicalBowlers(matches, deliveriesFile, year) {
    return new Promise(async function (resolve, reject) {
        let runsConcededByPlayer = {};
        let runsGiven = {};
        let matchesID = await getMatchID(matches, year);
        fs.readFile(deliveriesFile, function (err, data) {
            if (err) {
                reject(err)
            } else {
                data.toString().split("\n").forEach(function (line, index, arr) {
                    if (index !== 0) {
                        const delivery = line.split(",");
                        if (matchesID.includes(delivery[0])) {
                            const bowlerName = delivery[8];
                            const runsConceded = delivery[17];
                            let arr = [];
                            if (runsConcededByPlayer.hasOwnProperty(bowlerName)) {
                                arr = [runsConcededByPlayer[bowlerName][0] + 1, runsConcededByPlayer[bowlerName][1] + Number(runsConceded)]
                            } else {
                                arr = [1, Number(runsConceded)];
                            }
                            runsConcededByPlayer[bowlerName] = arr;
                        }
                    }
                })
            }
            for (let key in runsConcededByPlayer) {
                let economyRate = (runsConcededByPlayer[key][1] / runsConcededByPlayer[key][0]) * 6;
                runsConcededByPlayer[key] = economyRate;
            }
            var arr = [];
            var prop;
            for (prop in runsConcededByPlayer) {
                if (runsConcededByPlayer.hasOwnProperty(prop)) {
                    arr.push({
                        'key': prop,
                        'value': runsConcededByPlayer[prop]
                    });
                }
            }
            arr.sort(function (a, b) {
                return a.value - b.value;
            });
            let sortedArr = arr.splice(0, 10);
            resolve(sortedArr);
        })
    })
}


//5. Find the top scoring batsmen through the IPL season.
function getMostMOTM(matches) {
    return new Promise(function (resolve, reject) {
        let manOfTheMatch = {};
        fs.readFile(matches, function (err, data) {
            if (err) {
                reject(err);
            } else {
                data.toString().split("\n").forEach(function (line, index, arr) {
                    if (index != 0) {
                        const matchData = line.split(",");
                        const playerOfTheMatch = matchData[13];
                        if (manOfTheMatch.hasOwnProperty(playerOfTheMatch)) {
                            manOfTheMatch[playerOfTheMatch]++;
                        } else {
                            manOfTheMatch[playerOfTheMatch] = 1;
                        }
                    }
                })
            }

            var temp = [];
            var NameOfPlayer;
            for (NameOfPlayer in manOfTheMatch) {
                if (manOfTheMatch.hasOwnProperty(NameOfPlayer)) {
                    temp.push({
                        'key': NameOfPlayer,
                        'value': manOfTheMatch[NameOfPlayer]
                    });
                }
            }
            temp.sort(function (a, b) {
                return b.value - a.value;
            });
            let sortedTemp = temp.splice(0, 15);
            resolve(sortedTemp);
        })
    })
}

module.exports = {
    getMatchNumbers: getMatchNumbers,
    extraRuns: extraRuns,
    getMatchID: getMatchID,
    getMatchesWon: getMatchesWon,
    getEconimicalBowlers: getEconimicalBowlers,
    getMostMOTM: getMostMOTM
}