let expect = require("chai").expect;
let path = require("path");
let file = path.resolve("app.js");
let match = require(file);

describe("IPL Data Set Test Cases...", function () {

    it("Number of matches played per year of all the years in IPL.", function (done) {
        let file = path.resolve("testCSV/question1test.csv");

        const expectedResult = {
            2016: 2,
            2017: 2,
            2018: 1
        };

        match.getMatchNumbers(file).then(function (data) {
            try {
                expect(data).deep.equals(expectedResult);
                done();
            } catch (e) {
                done(e);
            }
        })
    })


    it("Matches won of all teams over all the years of IPL", function (done) {
        let file = path.resolve("testCSV/question2test.csv");
        const expectedResult = {
            'Kolkata Knight Riders': {
                '2010': 1,
                '2017': 1
            },
            'Mumbai Indians': {
                '2010': 1,
                '2017': 2
            },
            'Royal Challengers Bangalore': {
                '2008': 1,
                '2009': 1
            },
            'Kings XI Punjab': {
                '2008': 1
            },
            'Deccan Chargers': {
                '2009': 1
            }
        };

        match.getMatchesWon(file).then(function (data) {
            try {
                expect(data).deep.equals(expectedResult);
                done();
            } catch (e) {
                done(e);
            }
        })
    })


    it("Get the match IDs", function (done) {
        let matches = path.resolve("testCSV/matchIDtest.csv");
        let year = "2016";
        const expectedMatchIDs = ["1", "2", "4", "5", "7", "8", "10"];

        match.getMatchID(matches, year).then(function (data) {
            try {
                expect(data).deep.equals(expectedMatchIDs);
                matchIDs = data;
                done();
            } catch (e) {
                done(e);
            }
        })
    })


    it("For the year 2016 plot the extra runs conceded per team.", async function () {
        let deliveriesFileName = path.resolve("testCSV/question3d.csv");
        const match_ids = [1, 2]
        const expectedResult = {
            'Sunrisers Hyderabad': 6,
            'Royal Challengers Bangalore': 3
        }
        const result = await match.extraRuns(deliveriesFileName, match_ids)
        expect(result).deep.equals(expectedResult);
    })


    it("Get the most economical bowlers", async function () {
        let deliveriesFile = path.resolve("testCSV/question4testdeliveries.csv");
        const match_ids = ["531", "532", "533", "534", "535"];
        const expectedResult = [{
                key: 'CH Morris',
                value: 4.75
            },
            {
                key: 'STR Binny',
                value: 5
            },
            {
                key: 'B Kumar',
                value: 5.28
            },
            {
                key: 'IC Pandey',
                value: 6
            },
            {
                key: 'UT Yadav',
                value: 6
            },
            {
                key: 'Sandeep Sharma',
                value: 6.25
            },
            {
                key: 'JP Faulkner',
                value: 6.48
            },
            {
                key: 'YS Chahal',
                value: 6.720000000000001
            },
            {
                key: 'Harbhajan Singh',
                value: 6.75
            },
            {
                key: 'JP Duminy',
                value: 6.959999999999999
            }
        ];
        const result = await match.getEconimicalBowlers(deliveriesFile, match_ids)
        expect(result).deep.equals(expectedResult);
    })


    it("Most times man of the match.", async function () {
        let file = path.resolve("testCSV/question5test.csv");
        const expectedResult = [{
                key: 'Yuvraj Singh',
                value: 3
            },
            {
                key: 'GJ Maxwell',
                value: 1
            }
        ];
        const result = await match.getMostMOTM(file);
        expect(result).deep.equals(expectedResult);
    })
})