$(document).ready(function () {

    //First question.
    fetch("getMatchNumbers")
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {

            var chart = {
                type: "column"
            };

            var title = {
                text: "Number of matches played per year of all the years in IPL."
            };
            
            var xAxis = {
                
                catrgories: Object.keys(myJson),
                crosshair: true
            };

            var yAxis = {
                min: 0,
                title: {
                    text: "Matches played"
                }
            };

            var plotOptions = {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            };

            var series = [{
                name: ["Seasons"],
                data: Object.values(myJson)
            }];

            var json = {};
            json.chart = chart;
            json.title = title;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.series = series;
            json.plotOptions = plotOptions;
            $("#chart1").highcharts(json);
        });


    //Second Question
    fetch("getMatchesWon")
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {
            let teamNames = Object.keys(myJson);
            let seriesData = [];
            let years = new Set();
            for (var i = 0; i < teamNames.length; i++) {
                let data = [];
                for (let year = 2008; year < 2018; year++) {
                    if (myJson[teamNames[i]].hasOwnProperty(year))
                        data.push(myJson[teamNames[i]][year])
                    else
                        data.push(0)
                }
                seriesData.push({
                    name: teamNames[i],
                    data: data
                });
                Object.keys(myJson[teamNames[i]]).forEach((year) => {
                    years.add(year);
                })
            }

            var chart = {
                type: "bar"
            };

            var title = {
                text: "Matches won by each team in each season"
            };

            var xAxis = {
                categories: Array.from(years).sort(),
                // catrgories: Object.keys(myJson),
                crosshair: true
            };

            var yAxis = {
                min: 0,
                title: {
                    text: "Matches played"
                }
            };

            var plotOptions = {
                series: {
                    stacking: "normal"
                }
            };

            var json = {};
            json.chart = chart;
            json.title = title;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.series = seriesData;
            json.plotOptions = plotOptions;
            $("#chart2").highcharts(json);
        });


    // Third Question
    fetch("extraRuns")
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {
            var chart = {
                type: "column"
            };

            var title = {
                text: "Extra Runs Conceded Per Team in the year 2016"
            };

            var xAxis = {
                categories: Object.keys(myJson),
                crosshair: true
            };

            var yAxis = {
                min: 0,
                title: {
                    text: "Extra Runs Conceded"
                }
            };

            var plotOptions = {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            };

            var series = [{
                name: ["Teams"],
                data: Object.values(myJson)
            }];


            var json = {};
            json.chart = chart;
            json.title = title;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.series = series;
            json.plotOptions = plotOptions;
            $("#chart3").highcharts(json);

        });


    //Fourth Question
    fetch('/getEconimicalBowlers')
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {
            var chart = {
                type: 'column'
            };
            var title = {
                text: 'Top Economical Bowlers'
            };
            var subtitle = {
                text: 'Source: Ipl  (Kaggle)'
            };
            var xAxis = {
                categories: myJson.map(a => a.key),
                crosshair: true
            };
            var yAxis = {
                min: 0,
                title: {
                    text: 'Economy Rate'
                }
            };
            var plotOptions = {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            };
            var series = [{
                name: ['Players'],
                data: myJson.map(a => a.value)
            }];

            var json = {};
            json.chart = chart;
            json.title = title;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.series = series;
            json.plotOptions = plotOptions;
            $('#chart4').highcharts(json);
        });


    //Fifth question.
    fetch('/getMostMOTM')
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {

            var chart = {
                type: 'column'
            };
            var title = {
                text: "Most Man Of The Match Awards"
            };

            var xAxis = {
                categories: myJson.map(a => a.key),
                crosshair: true
            };

            var yAxis = {
                min: 0,
                title: {
                    text: 'Man Of The Match Awards'
                }
            };

            var plotOptions = {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            };

            var series = [{
                name: ['Players'],
                data: myJson.map(a => a.value)
            }];

            var json = {};
            json.chart = chart;
            json.title = title;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.series = series;
            json.plotOptions = plotOptions;
            $('#chart5').highcharts(json);
        });
})